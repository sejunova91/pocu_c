#pragma once

#include <memory>
#include <vector>
#include <algorithm>

namespace assignment4
{
	template<typename T>
	class TreeNode;

	template<typename T>
	class BinarySearchTree final
	{
	public:
		void Insert(std::unique_ptr<T> data);
		bool Search(const T& data);
		bool Delete(const T& data);
		const std::weak_ptr<TreeNode<T>> GetRootNode() const;

		static std::vector<T> TraverseInOrder(const std::shared_ptr<TreeNode<T>> startNode);

	private:
		void InsertIntoBinarySearchTree(std::shared_ptr<TreeNode<T>> node, std::unique_ptr<T> data);
		static void PushIntoVector(std::shared_ptr<TreeNode<T>> node, std::vector<T>& v);
		static const std::shared_ptr<TreeNode<T>> SearchBinaryTree(const std::shared_ptr<TreeNode<T>> node, const T& data);
		void DeleteNode(std::shared_ptr<TreeNode<T>> node);

		std::shared_ptr<TreeNode<T>> mRootNode;
	};

	template<typename T>
	void BinarySearchTree<T>::Insert(std::unique_ptr<T> data)
	{
		if (mRootNode == nullptr)
		{
			mRootNode = std::make_shared<TreeNode<T>>(std::move(data));
		}
		else
		{
			InsertIntoBinarySearchTree(mRootNode, std::move(data));
		}
	}
	
	template<typename T>
	void BinarySearchTree<T>::InsertIntoBinarySearchTree(std::shared_ptr<TreeNode<T>> currentNode, std::unique_ptr<T> data)
	{
		if (*data <= *currentNode->Data)
		{
			if (currentNode->Left == nullptr)
			{
				currentNode->Left = std::make_shared<TreeNode<T>>(currentNode, std::move(data));
			}
			else
			{
				InsertIntoBinarySearchTree(currentNode->Left, std::move(data));
			}
		}
		else
		{
			if (currentNode->Right == nullptr)
			{
				currentNode->Right = std::make_shared<TreeNode<T>>(currentNode, std::move(data));
			}
			else
			{
				InsertIntoBinarySearchTree(currentNode->Right, std::move(data));
			}
		}
	}

	template<typename T>
	const std::weak_ptr<TreeNode<T>> BinarySearchTree<T>::GetRootNode() const
	{
		const std::weak_ptr<TreeNode<T>> rootNode = mRootNode;
		return rootNode;
	}

	template<typename T>
	bool BinarySearchTree<T>::Search(const T& data)
	{
		if (mRootNode == nullptr)
		{
			return false;
		}

		std::shared_ptr<TreeNode<T>> nodeFound = SearchBinaryTree(mRootNode, data);
		if (nodeFound != nullptr)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	template<typename T>
	const std::shared_ptr<TreeNode<T>> BinarySearchTree<T>::SearchBinaryTree(const std::shared_ptr<TreeNode<T>> currentNode, const T& data)
	{
		if (data == *currentNode->Data)
		{
			return currentNode;
		}
		else
		{
			if (data > *currentNode->Data && currentNode->Right != nullptr)
			{
				return SearchBinaryTree(currentNode->Right, data);
			}
			else if (data < *currentNode->Data && currentNode->Left != nullptr)

			{
				return SearchBinaryTree(currentNode->Left, data);
			}
			else
			{
				return nullptr;
			}
		}
	}

	template<typename T>
	bool BinarySearchTree<T>::Delete(const T& data)
	{
		if (mRootNode == nullptr)
		{
			return false;
		}

		std::shared_ptr<TreeNode<T>> nodeFound = SearchBinaryTree(mRootNode, data);
		if (nodeFound == nullptr)
		{
			return false;
		}
		
		if (!(nodeFound->Left != nullptr && nodeFound->Right != nullptr))
		{
			DeleteNode(nodeFound);
		}
		else
		{
			std::shared_ptr<TreeNode<T>> smallestNodeBelowNodeFound = nodeFound->Right;
			while (true)
			{
				if (smallestNodeBelowNodeFound->Left == nullptr)
				{
					break;
				}
				smallestNodeBelowNodeFound = smallestNodeBelowNodeFound->Left;
			}

			// swap
			nodeFound->Data.swap(smallestNodeBelowNodeFound->Data);
			DeleteNode(smallestNodeBelowNodeFound);
		}

		return true;
	}

	template<typename T>
	void BinarySearchTree<T>::DeleteNode(std::shared_ptr<TreeNode<T>> node)
	{

		// 1. 찾은 노드가 가장 하위 레벨에 위치할 때. (왼쪽, 오른쪽 노드 모두 없음)
		if (node->Left == nullptr && node->Right == nullptr)
		{
			if (node->Parent.expired())
			{
				mRootNode = nullptr;
			}
			else
			{
				std::shared_ptr<TreeNode<T>> parent = node->Parent.lock();
				if (node == parent->Left)
				{
					parent->Left = nullptr;
				}
				else
				{
					parent->Right = nullptr;
				}
			}
		}

		// 2. 찾은 노드의 왼쪽, 혹은 오른쪽 노드만 존재하는 경우
		else if (node->Left != nullptr && node->Right == nullptr)
		{
			if (node->Parent.expired())
			{
				mRootNode = node->Left;
			}
			else
			{
				std::shared_ptr<TreeNode<T>> parent = node->Parent.lock();
				node->Left->Parent = parent;
				if (parent->Left == node)
				{
					parent->Left = node->Left;
				}
				else
				{
					parent->Right = node->Left;
				}
			}
		}
		else if (node->Right != nullptr && node->Left == nullptr)
		{
			if (node == mRootNode)
			{
				mRootNode = node->Right;
			}
			else
			{
				std::shared_ptr<TreeNode<T>> parent = node->Parent.lock();
				node->Right->Parent = parent;
				if (parent->Right == node)
				{
					parent->Right = node->Right;
				}
				else
				{
					parent->Left = node->Right;
				}
			}
		}
	}

	template<typename T>
	std::vector<T> BinarySearchTree<T>::TraverseInOrder(const std::shared_ptr<TreeNode<T>> startNode)
	{
		std::vector<T> v;
		if (startNode != nullptr)
		{
			PushIntoVector(startNode, v);
			std::sort(v.begin(), v.end());
		}
		return v;
	}

	template<typename T>
	void BinarySearchTree<T>::PushIntoVector(std::shared_ptr<TreeNode<T>> currentNode, std::vector<T>& v)
	{
		if (currentNode != nullptr)
		{
			v.push_back(*currentNode->Data);
			PushIntoVector(currentNode->Left, v);
			PushIntoVector(currentNode->Right, v);
		}
	}
}