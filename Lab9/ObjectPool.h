#pragma once
#include <queue>

namespace lab9
{
	template<typename T>
	class ObjectPool
	{
	public:
		ObjectPool(size_t maxPoolSize);
		ObjectPool(const ObjectPool&) = delete;
		ObjectPool& operator=(const ObjectPool&) = delete;
		~ObjectPool();
		T* Get();
		void Return(T* t);
		size_t GetFreeObjectCount();
		size_t GetMaxFreeObjectCount();
	private:
		std::queue<T*> mObjectQueue;
		size_t mMaxPoolSize;
	};

	template<typename T> ObjectPool<T>::ObjectPool(size_t maxPoolSize)
		: mMaxPoolSize(maxPoolSize)
	{
	}

	template<typename T> ObjectPool<T>::~ObjectPool()
	{
		size_t queueLength = mObjectQueue.size();
		for (size_t i = 0; i < queueLength; i++)
		{
			T* t = mObjectQueue.front();
			delete t;
			mObjectQueue.pop();

		}
	}

	template<typename T>
	T* ObjectPool<T>::Get()
	{
		if (mObjectQueue.size() == 0)
		{
			return new T();
		}
		T* object = mObjectQueue.front();
		mObjectQueue.pop();
		return object;
	}

	template<typename T>
	void ObjectPool<T>::Return(T* t)
	{
		if (mObjectQueue.size() == mMaxPoolSize)
		{
			delete t;
			return;
		}
		mObjectQueue.push(t);
	}

	template<typename T>
	size_t ObjectPool<T>::GetFreeObjectCount()
	{
		return mObjectQueue.size();
	}

	template<typename T>
	size_t ObjectPool<T>::GetMaxFreeObjectCount()
	{
		return mMaxPoolSize;
	}
}
