#include <stack>
#include <limits>
#include <math.h>

namespace assignment3
{
	template<typename T>
	class SmartStack
	{
	public:
		SmartStack();
		void Push(const T number);
		T Pop();
		T Peek() const;
		T Max() const;
		T Min() const;
		T Sum()const;
		double Average() const;
		double Variance() const;
		double StandardDeviation() const;
		unsigned int Count() const;

	private:
		std::stack<T> mStack;
		std::stack<T> mMinStack;
		std::stack<T> mMaxStack;
		T mSum;
		double mSquareSum;
	};

	template<typename T> SmartStack<T>::SmartStack()
		: mSum(0)
		, mSquareSum(0)
	{
	}

	template<typename T>
	void SmartStack<T>::Push(const T number)
	{
		if (mStack.size() == 0)
		{
			mMaxStack.push(number);
			mMinStack.push(number);
		}
		else
		{
			if (number > mMaxStack.top())
			{
				mMaxStack.push(number);
			}
			else
			{
				mMaxStack.push(mMaxStack.top());
			}
			if (number < mMinStack.top())
			{
				mMinStack.push(number);
			}
			else
			{
				mMinStack.push(mMinStack.top());
			}
		}
		mStack.push(number);
		mSum += number;
		mSquareSum += (static_cast<double>(number) * static_cast<double>(number));
	}

	template<typename T>
	unsigned int SmartStack<T>::Count() const
	{
		return static_cast<unsigned int>(mStack.size());
	}

	template<typename T>
	T SmartStack<T>::Peek() const
	{
		return mStack.top();
	}

	template<typename T>
	T SmartStack<T>::Pop()
	{
		T popped = mStack.top();
		mStack.pop();
		mSum -= popped;
		mSquareSum -= (static_cast<double>(popped) * static_cast<double>(popped));

		mMinStack.pop();
		mMaxStack.pop();

		return popped;
	}

	template<typename T>
	T SmartStack<T>::Max() const
	{
		if (mStack.size() == 0)
		{
			return std::numeric_limits<T>::lowest();
		}
		return mMaxStack.top();
	}

	template<typename T>
	T SmartStack<T>::Min() const
	{
		if (mStack.size() == 0)
		{
			return std::numeric_limits<T>::max();
		}
		return mMinStack.top();
	}

	template<typename T>
	T SmartStack<T>::Sum() const
	{
		return mSum;
	}

	template<typename T>
	double SmartStack<T>::Average() const
	{
		double average = static_cast<double>(mSum) / mStack.size();
		return round(average * 1000) / 1000;
	}

	template<typename T>
	double SmartStack<T>::Variance() const
	{
		double average = static_cast<double>(mSum) / mStack.size();
		double variance = mSquareSum / mStack.size() - (average * average);
		return round(variance * 1000) / 1000;
	}

	template<typename T>
	double SmartStack<T>::StandardDeviation() const
	{
		double average = static_cast<double>(mSum) / mStack.size();
		double variance = mSquareSum / mStack.size() - (average * average);
		double standardDeviation = sqrt(variance);
		return round(standardDeviation * 1000) / 1000;
	}
}
