#include <queue>
#include <limits>
#include <math.h>

namespace assignment3
{
	template<typename T>
	class SmartQueue
	{
	public:
		SmartQueue();
		void Enqueue(const T number);
		T Peek() const;
		T Dequeue();
		T Max();
		T Min();
		T Sum() const;
		double Average() const;
		double Variance() const;
		double StandardDeviation() const;
		unsigned int Count() const;

	private:
		std::queue<T> mQueue;
		T mSum;
		double mSquareSum;
	};

	template<typename T> SmartQueue<T>::SmartQueue()
		: mSum(0)
		, mSquareSum(0)
	{
	}

	template<typename T>
	void SmartQueue<T>::Enqueue(const T number)
	{
		mQueue.push(number);
		mSum += number;
		mSquareSum += (static_cast<double>(number) * static_cast<double>(number));
	}

	template<typename T>
	unsigned int SmartQueue<T>::Count() const
	{
		return static_cast<unsigned int>(mQueue.size());
	}

	template<typename T>
	T SmartQueue<T>::Peek() const
	{
		return mQueue.front();
	}

	template<typename T>
	T SmartQueue<T>::Dequeue()
	{
		T popped = mQueue.front();
		mQueue.pop();
		mSum -= popped;
		mSquareSum -= (static_cast<double>(popped) * static_cast<double>(popped));
		return popped;
	}

	template<typename T>
	T SmartQueue<T>::Max()
	{
		T max = std::numeric_limits<T>::lowest();
		if (mQueue.size() == 0)
		{
			return max;
		}

		for (size_t i = 0; i < mQueue.size(); ++i)
		{
			T popped = mQueue.front();
			mQueue.pop();
			mQueue.push(popped);

			if (popped > max)
			{
				max = popped;
			}
		}
		return max;
	}

	template<typename T>
	T SmartQueue<T>::Min()
	{
		T min = std::numeric_limits<T>::max();
		if (mQueue.size() == 0)
		{
			return min;
		}

		for (size_t i = 0; i < mQueue.size(); ++i)
		{
			T popped = mQueue.front();
			mQueue.pop();
			mQueue.push(popped);

			if (popped < min)
			{
				min = popped;
			}
		}
		return min;
	}

	template<typename T>
	T SmartQueue<T>::Sum() const
	{
		return mSum;
		//        return static_cast<T>(round(mSum * 1000) / 1000);
	}

	template<typename T>
	double SmartQueue<T>::Average() const
	{
		double average = static_cast<double>(mSum) / mQueue.size();
		return round(average * 1000) / 1000;
	}

	template<typename T>
	double SmartQueue<T>::Variance() const
	{
		double average = static_cast<double>(mSum) / mQueue.size();
		double variance = mSquareSum / mQueue.size() - (average * average);
		return round(variance * 1000) / 1000;
	}

	template<typename T>
	double SmartQueue<T>::StandardDeviation() const
	{
		double average = static_cast<double>(mSum) / mQueue.size();
		double variance = mSquareSum / mQueue.size() - (average * average);
		double standardDeviation = sqrt(variance);
		return round(standardDeviation * 1000) / 1000;
	}
}
