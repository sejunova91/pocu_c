#include <queue>
#include <stack>
#include <limits>
#include <math.h>

namespace assignment3
{
	template<typename T>
	class QueueStack
	{
	public:
		QueueStack(const unsigned int maxStackSize);
		void Enqueue(const T number);
		T Peek() const;
		T Dequeue();
		T Max();
		T Min();
		T Sum() const;
		double Average() const;
		unsigned int Count() const;
		unsigned int StackCount() const;

	private:
		std::queue<std::stack<T>> mQueueStack;
		T mSum;
		T mSquareSum;
		unsigned int mCount;
		const unsigned int mMaxStackSize;
	};

	template<typename T> QueueStack<T>::QueueStack(const unsigned int maxStackSize)
		: mSum(0)
		, mSquareSum(0)
		, mCount(0)
		, mMaxStackSize(maxStackSize)
	{
	}

	template<typename T>
	void QueueStack<T>::Enqueue(const T number)
	{
		if (mCount == 0 || mQueueStack.back().size() == mMaxStackSize)
		{
			mQueueStack.push(std::stack<T>());
		}

		std::stack<T> lastStack = mQueueStack.back();
		lastStack.push(number);

		mQueueStack.back().push(number);
		mCount++;
		mSum += number;
		mSquareSum += (number * number);
	}

	template<typename T>
	unsigned int QueueStack<T>::Count() const
	{
		return mCount;
	}

	template<typename T>
	unsigned int QueueStack<T>::StackCount() const
	{
		return static_cast<unsigned int>(mQueueStack.size());
	}

	template<typename T>
	T QueueStack<T>::Peek() const
	{
		return mQueueStack.front().top();
	}

	template<typename T>
	T QueueStack<T>::Dequeue()
	{
		T popped = mQueueStack.front().top();
		if (mQueueStack.front().size() == 1)
		{
			mQueueStack.pop();
		}
		else
		{
			mQueueStack.front().pop();
		}
		mCount--;
		mSum -= popped;
		mSquareSum -= (popped * popped);
		return popped;
	}

	template<typename T>
	T QueueStack<T>::Max()
	{
		T max = std::numeric_limits<T>::lowest();
		if (mCount == 0)
		{
			return max;
		}

		const size_t QUEUE_STACK_SIZE = mQueueStack.size();
		for (size_t i = 0; i < QUEUE_STACK_SIZE; ++i)
		{
			std::stack<T> poppedStack = mQueueStack.front();
			mQueueStack.pop();
			mQueueStack.push(poppedStack);

			const size_t POPPED_STACK_SIZE = poppedStack.size();
			for (size_t j = 0; j < POPPED_STACK_SIZE; ++j)
			{
				T popped = poppedStack.top();
				poppedStack.pop();
				if (popped > max)
				{
					max = popped;
				}
			}
		}
		return max;
	}

	template<typename T>
	T QueueStack<T>::Min()
	{
		T min = std::numeric_limits<T>::max();
		if (mCount == 0)
		{
			return min;
		}
		const size_t QUEUE_STACK_SIZE = mQueueStack.size();
		for (size_t i = 0; i < QUEUE_STACK_SIZE; ++i)
		{
			std::stack<T> poppedStack = mQueueStack.front();
			mQueueStack.pop();
			mQueueStack.push(poppedStack);

			const size_t POPPED_STACK_SIZE = poppedStack.size();
			for (size_t j = 0; j < POPPED_STACK_SIZE; ++j)
			{
				T popped = poppedStack.top();
				poppedStack.pop();
				if (popped < min)
				{
					min = popped;
				}
			}
		}
		return min;
	}

	template<typename T>
	T QueueStack<T>::Sum() const
	{
		return mSum;
	}

	template<typename T>
	double QueueStack<T>::Average() const
	{
		double average = static_cast<double>(mSum) / mCount;
		return round(average * 1000) / 1000;
	}
}