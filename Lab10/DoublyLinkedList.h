#pragma once

#include <memory>
#include <iostream>

namespace lab10
{
	template<typename T>
	class Node;

	template<typename T>
	class DoublyLinkedList
	{
	public:
		DoublyLinkedList();
		void Insert(std::unique_ptr<T> data);
		void Insert(std::unique_ptr<T> data, unsigned int index);
		bool Delete(const T& data);
		bool Search(const T& data) const;
		void PrintAll() const;

		std::shared_ptr<Node<T>> operator[](unsigned int index) const;
		unsigned int GetLength() const;

	private:
		std::shared_ptr<Node<T>> mHead;
		unsigned int mLength;
	};

	template<typename T> DoublyLinkedList<T>::DoublyLinkedList()
		: mLength(0)
	{
	}

	template<typename T>
	void DoublyLinkedList<T>::Insert(std::unique_ptr<T> data)
	{
		if (mLength == 0)
		{
			mHead = std::make_shared<Node<T>>(std::move(data));
		}
		else
		{
			std::shared_ptr<Node<T>> node = mHead;
			while (node->Next != nullptr)
			{
				node = node->Next;
			}
			std::shared_ptr<Node<T>> newNode = std::make_shared<Node<T>>(std::move(data), node);
			node->Next = newNode;
		}
		mLength++;
	}

	template<typename T>
	void DoublyLinkedList<T>::Insert(std::unique_ptr<T> data, unsigned int index)
	{
		if (mLength == 0)
		{
			mHead = std::make_shared<Node<T>>(std::move(data));
			mLength++;
			return;
		}

		if (index == 0)
		{
			std::shared_ptr<Node<T>> newHead = std::make_shared<Node<T>>(std::move(data));
			newHead->Next = mHead;
			mHead->Previous = newHead;
			mHead = newHead;
			mLength++;
			return;
		}

		if (index >= mLength)
		{
			std::shared_ptr<Node<T>> node = mHead;
			while (node->Next != nullptr)
			{
				node = node->Next;
			}
			std::shared_ptr<Node<T>> newNode = std::make_shared<Node<T>>(std::move(data), node);
			node->Next = newNode;
			mLength++;
			return;
		}

		std::shared_ptr<Node<T>> previous = mHead;
		for (unsigned int i = 0; i < index - 1; i++)
		{
			previous = previous->Next;
		}
		std::shared_ptr<Node<T>> newNode = std::make_shared<Node<T>>(std::move(data), previous);
		newNode->Next = previous->Next;
		newNode->Next->Previous = newNode;
		previous->Next = newNode;
		mLength++;
	}

	template<typename T>
	bool DoublyLinkedList<T>::Delete(const T& data)
	{
		if (mLength == 0)
		{
			return false;
		}

		std::shared_ptr<Node<T>> node = mHead;
		while (true)
		{
			if (node == nullptr)
			{
				break;
			}

			if (*(node->Data) == data)
			{
				if (node->Previous.expired())
				{
					mHead = node->Next;
				}
				else if (node->Next == nullptr)
				{
					node->Previous.lock()->Next = nullptr;
				}
				else
				{
					node->Next->Previous = node->Previous;
					auto previous = node->Previous.lock();
					previous->Next = node->Next;
				}
				mLength--;
				return true;
			}
			node = node->Next;
		}
		return false;
	}

	template<typename T>
	bool DoublyLinkedList<T>::Search(const T& data) const
	{
		if (mLength == 0)
		{
			return false;
		}
		std::shared_ptr<Node<T>> node = mHead;
		while (true)
		{
			if (node == nullptr)
			{
				break;
			}

			if (*(node->Data) == data)
			{
				return true;
			}
			node = node->Next;
		}
		return false;
	}

	template<typename T>
	std::shared_ptr<Node<T>> DoublyLinkedList<T>::operator[](unsigned int index) const
	{
		if (index >= mLength)
		{
			return nullptr;
		}

		std::shared_ptr<Node<T>> current = mHead;
		for (unsigned int i = 0; i < index; i++)
		{
			current = current->Next;
		}
		return current;
	}

	template<typename T>
	unsigned int DoublyLinkedList<T>::GetLength() const
	{
		return mLength;
	}

	template<typename T>
	void DoublyLinkedList<T>::PrintAll() const
	{
		std::shared_ptr<Node<T>> node = mHead;
		while (node->Next != nullptr)
		{
			std::cout << *(node->Data) << std::endl;
			node = node->Next;
		}
		std::cout << *(node->Data) << std::endl;
	}
}
