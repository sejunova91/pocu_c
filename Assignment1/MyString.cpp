#include "MyString.h"

namespace assignment1
{
	unsigned int getStringLength(const char* s)
	{
		unsigned int i = 0;
		while (*s != '\0')
		{
			i++;
			s++;
		}
		return i;
	}
	bool stringCompare(const char* aString, const char* bString)
	{
		int aStringLength = getStringLength(aString);
		int bStringLength = getStringLength(bString);
		if (aStringLength != bStringLength)
		{
			return false;
		}
		while (*aString != '\0')
		{
			if (*aString != *bString)
			{
				return false;
			}
			aString++;
			bString++;
		}
		return true;
	}

	MyString::MyString(const char* s)
	{
		mLength = getStringLength(s);
		mString = new char[mLength + 1];
		for (unsigned int j = 0; j <= mLength; ++j)
		{
			mString[j] = s[j];
		}
	}

	MyString::MyString(const MyString& other)
	{
		mLength = other.mLength;
		mString = new char[other.mLength + 1];
		for (unsigned int j = 0; j <= other.mLength; ++j)
		{
			mString[j] = other.mString[j];
		}
	}

	MyString::~MyString()
	{
		delete[] mString;
	}

	unsigned int MyString::GetLength() const
	{
		return mLength;
	}

	const char* MyString::GetCString() const
	{
		const char* string;
		string = mString;
		return string;
	}

	void MyString::Append(const char* s)
	{
		unsigned int appendStringLength;
		unsigned int newStringLength;
		char* newString;

		if (s[0] == '\0')
		{
		}
		else
		{
			appendStringLength = getStringLength(s);
			newStringLength = mLength + appendStringLength;

			newString = new char[newStringLength + 1];
			for (unsigned int j = 0; j <= newStringLength; ++j)
			{
				if (j < mLength)
				{
					newString[j] = mString[j];
				}
				else
				{
					newString[j] = s[j - mLength];
				}
			}
			delete[] mString;
			mString = newString;
			mLength = newStringLength;
		}
	}

	MyString MyString::operator+(const MyString& other) const
	{
		unsigned int newStringLength = mLength + other.mLength;
		char* newString;

		newString = new char[newStringLength + 1];
		for (unsigned int i = 0; i <= newStringLength; ++i)
		{
			if (i < mLength)
			{
				newString[i] = mString[i];
			}
			else
			{
				newString[i] = other.mString[i - mLength];
			}
		}
		MyString newMyString = MyString(newString);
		delete[] newString;
		return newMyString;
	}

	int MyString::IndexOf(const char* s)
	{
		const char* sFirstChar = s;
		unsigned int findStringLength = getStringLength(s);
		if (findStringLength == 0)
		{
			return 0;
		}
		if (findStringLength > mLength)
		{
			return -1;
		}
		for (unsigned int i = 0; i <= mLength - findStringLength; ++i)
		{
			for (unsigned int j = 0; j < findStringLength; ++j)
			{
				if (mString[i + j] == *s)
				{
					if (j + 1 == findStringLength)
					{
						return i;
					}
					else
					{
						s++;
					}
				}
				else
				{
					s = sFirstChar;
					break;
				}
			}
		}
		return -1;
	}

	int MyString::LastIndexOf(const char* s)
	{
		const char* sFirstChar = s;
		unsigned int findStringLength = getStringLength(s);
		if (findStringLength == 0)
		{
			if (mLength == 0)
			{
				return 0;
			}
			else
			{
				return mLength;
			}
		}
		if (findStringLength > mLength)
		{
			return -1;
		}

		for (int i = mLength - findStringLength; i >= 0; --i)
		{
			for (unsigned int j = 0; j < findStringLength; ++j)
			{
				if (mString[i + j] == *s)
				{
					if (j + 1 == findStringLength)
					{
						return i;
					}
					else
					{
						s++;
					}
				}
				else
				{
					s = sFirstChar;
					break;
				}
			}
		}
		return -1;
	}

	void MyString::Interleave(const char* s)
	{
		char* newString;
		unsigned int sLength;
		unsigned int bigCharLength;
		unsigned int currentIndex;
		unsigned int newStringLength;
		sLength = getStringLength(s);
		if (sLength == 0)
		{
		}
		else
		{
			if (sLength > mLength)
			{
				bigCharLength = sLength;
			}
			else
			{
				bigCharLength = mLength;
			}
			newStringLength = sLength + mLength;
			newString = new char[newStringLength + 1];
			currentIndex = 0;
			for (unsigned int i = 0; i < bigCharLength; ++i)
			{
				if (i < mLength)
				{
					newString[currentIndex] = mString[i];
					currentIndex++;
				}
				if (i < sLength)
				{
					newString[currentIndex] = s[i];
					currentIndex++;
				}
			}
			newString[newStringLength] = '\0';
			delete[] mString;
			mString = newString;
			mLength = newStringLength;
		}
	}

	bool MyString::RemoveAt(unsigned int index)
	{
		char* newString;
		int currentIndex;
		if (index >= mLength)
		{
			return false;
		}
		newString = new char[mLength];
		currentIndex = 0;
		for (unsigned int i = 0; i <= mLength; ++i)
		{
			if (i == index)
			{
				continue;
			}
			else
			{
				newString[currentIndex] = mString[i];
				currentIndex++;
			}
		}
		newString[mLength - 1] = '\0';
		delete[] mString;
		mString = newString;
		mLength--;
		return true;
	}

	void MyString::PadLeft(unsigned int totalLength)
	{
		unsigned int paddingSize;
		unsigned int currentIndex;
		if (mLength >= totalLength)
		{
		}
		else
		{
			paddingSize = totalLength - mLength;
			char* newString;
			newString = new char[totalLength + 1];
			currentIndex = 0;
			for (unsigned int i = 0; i < paddingSize; ++i)
			{
				newString[currentIndex] = ' ';
				currentIndex++;
			}
			for (unsigned int j = 0; j < mLength; ++j)
			{
				newString[currentIndex] = mString[j];
				currentIndex++;
			}
			newString[totalLength] = '\0';
			delete[] mString;
			mString = newString;
			mLength = totalLength;
		}
	}

	void MyString::PadLeft(unsigned int totalLength, const char c)
	{
		unsigned int paddingSize;
		unsigned int currentIndex;
		if (mLength >= totalLength)
		{
		}
		else
		{
			paddingSize = totalLength - mLength;
			char* newString;
			newString = new char[totalLength + 1];
			currentIndex = 0;
			for (unsigned int i = 0; i < paddingSize; ++i)
			{
				newString[currentIndex] = c;
				currentIndex++;
			}
			for (unsigned int j = 0; j < mLength; ++j)
			{
				newString[currentIndex] = mString[j];
				currentIndex++;
			}
			newString[totalLength] = '\0';
			delete[] mString;
			mString = newString;
			mLength = totalLength;
		}
	}

	void MyString::PadRight(unsigned int totalLength)
	{
		unsigned int paddingSize;
		unsigned int currentIndex;
		if (mLength >= totalLength)
		{
		}
		else
		{
			paddingSize = totalLength - mLength;
			char* newString;
			newString = new char[totalLength + 1];
			currentIndex = 0;
			for (unsigned int j = 0; j < mLength; ++j)
			{
				newString[currentIndex] = mString[j];
				currentIndex++;
			}
			for (unsigned int i = 0; i < paddingSize; ++i)
			{
				newString[currentIndex] = ' ';
				currentIndex++;
			}
			newString[totalLength] = '\0';
			delete[] mString;
			mString = newString;
			mLength = totalLength;
		}
	}

	void MyString::PadRight(unsigned int totalLength, const char c)
	{
		unsigned int paddingSize;
		unsigned int currentIndex;
		if (mLength >= totalLength)
		{
		}
		else
		{
			paddingSize = totalLength - mLength;
			char* newString;
			newString = new char[totalLength + 1];
			currentIndex = 0;
			for (unsigned int j = 0; j < mLength; ++j)
			{
				newString[currentIndex] = mString[j];
				currentIndex++;
			}
			for (unsigned int i = 0; i < paddingSize; ++i)
			{
				newString[currentIndex] = c;
				currentIndex++;
			}
			newString[totalLength] = '\0';
			delete[] mString;
			mString = newString;
			mLength = totalLength;
		}
	}

	void MyString::Reverse()
	{
		int j = mLength - 1;
		for (unsigned int i = 0; i < mLength / 2; ++i)
		{
			char temp = mString[i];
			mString[i] = mString[j];
			mString[j] = temp;
			j--;
		}
	}

	bool MyString::operator==(const MyString& rhs) const
	{
		if (stringCompare(mString, rhs.mString))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void MyString::ToLower()
	{
		const int ASCII_UPPER_BEGIN = 65;
		const int ASCII_UPPER_END = 90;
		for (unsigned int i = 0; i < mLength; ++i)
		{
			if (ASCII_UPPER_BEGIN <= mString[i] && ASCII_UPPER_END >= mString[i])
			{
				mString[i] = mString[i] + 32;
			}
		}
	}

	void MyString::ToUpper()
	{
		const int ASCII_LOWER_BEGIN = 97;
		const int ASCII_LOWER_END = 122;
		for (unsigned int i = 0; i < mLength; ++i)
		{
			if (ASCII_LOWER_BEGIN <= mString[i] && ASCII_LOWER_END >= mString[i])
			{
				mString[i] = mString[i] - 32;
			}
		}
	}
}
