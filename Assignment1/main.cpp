#include <iostream>
#include "MyString.h"

using namespace assignment1;
using namespace std;

void charTest() 
{
	cout << "CharClassTest" << endl;
	char const* normal_string = "abcd";
	MyString* MyNormalString = new MyString(normal_string);
	cout << MyNormalString->GetCString() << endl;
	cout << MyNormalString->GetLength() << endl;

	char const* no_string = "";
	MyString* MyNoString = new MyString(no_string);
	cout << MyNoString->GetCString() << endl;
	cout << MyNoString->GetLength() << endl;
	cout << "-----------------------------" << endl;
}
void stringClassTest() 
{
	cout << "StringClassTest" << endl;
	char const* normal_string = "hello";
	MyString MyNormalString = MyString(normal_string);
	MyString* MyNewNormalString = new MyString(MyNormalString);
	cout << MyNewNormalString->GetCString() << endl;
	cout << MyNewNormalString->GetLength() << endl;

	cout << "###### Append Test #######" << endl;
	const char* world = " world";
	MyNewNormalString->Append(world);
	cout << MyNewNormalString->GetCString() << endl;

	cout << "###### Operator + Test #######" << endl;
	MyString FirstString = MyString("PlusPlus");
	MyString SecondString = MyString("!!!");
	MyString noString = MyString("");
	MyString StringAdded = FirstString + SecondString;
	cout << StringAdded.GetCString() << endl;


	cout << "###### Interleave Test #######" << endl;
	MyString interLeave("Hello");
	interLeave.Interleave(" World");
	cout << interLeave.GetCString() << endl;

	interLeave.Interleave("");
	cout << interLeave.GetCString() << endl;

	cout << "###### RemoveAt Test #######" << endl;
	MyString removeAt("Hello");

	bool b1 = removeAt.RemoveAt(0);
	cout << b1 << endl;
	cout << removeAt.GetCString() << endl;

	bool b2 = removeAt.RemoveAt(2);
	cout << b2 << endl;
	cout << removeAt.GetCString() << endl;

	bool b3 = removeAt.RemoveAt(3);
	cout << b3 << endl;
	cout << removeAt.GetCString() << endl;

	cout << "###### PadLeft Test #######" << endl;
	MyString padLeft1("Hello");
	padLeft1.PadLeft(8);
	cout << padLeft1.GetCString() << endl;
	cout << padLeft1.GetLength() << endl;

	MyString padLeft2("Hello");
	padLeft2.PadLeft(8, '#');
	cout << padLeft2.GetCString() << endl;
	cout << padLeft2.GetLength() << endl;

	cout << "###### PadRight Test #######" << endl;
	MyString padRight1("Hello");
	padRight1.PadRight(8);
	cout << padRight1.GetCString() << endl;

	MyString padRight2("Hello");
	padRight2.PadRight(8, '#');
	cout << padRight2.GetCString() << endl;

	cout << "###### Reverse Test #######" << endl;
	MyString reverse("Hello");
	reverse.Reverse();
	cout << reverse.GetCString() << endl;

	cout << "###### Opeartor==  #######" << endl;
	MyString a("Hello");
	MyString b("Hello");
	MyString c("World");

	b1 = a == b;
	b2 = a == c;

	cout << b1 << endl;
	cout << b2 << endl;

	cout << "###### ToUpper  #######" << endl;
	MyString toUpper("Hello");
	toUpper.ToUpper();
	cout << toUpper.GetCString() << endl;

	cout << "###### ToLower  #######" << endl;
	MyString toLower("hELLO");
	toLower.ToLower();
	cout << toLower.GetCString() << endl;
	cin.get();
}
void indexTest()
{
	MyString FirstString = MyString("PlusPlus");
	MyString SecondString = MyString("!!!");
	MyString noString = MyString("");

	cout << "###### IndexOf Test #######" << endl;
	int index;
	index = FirstString.IndexOf("Plus");
	cout << index << endl;

	index = FirstString.IndexOf("lu");
	cout << index << endl;

	index = FirstString.IndexOf("u");
	cout << index << endl;

	index = FirstString.IndexOf("a");
	cout << index << endl;

	index = FirstString.IndexOf("PlusPlus1");
	cout << index << endl;

	index = FirstString.IndexOf("");
	cout << index << endl;

	index = noString.IndexOf("");
	cout << index << endl;

	index = noString.IndexOf("k");
	cout << index << endl;

	cout << "###### LastIndexOf Test #######" << endl;
	index = FirstString.LastIndexOf("PlusPlus");
	cout << index << endl;

	index = FirstString.LastIndexOf("Plus");
	cout << index << endl;

	index = FirstString.LastIndexOf("lu");
	cout << index << endl;

	index = FirstString.LastIndexOf("u");
	cout << index << endl;

	index = FirstString.LastIndexOf("a");
	cout << index << endl;

	index = FirstString.LastIndexOf("");
	cout << index << endl;

	index = noString.LastIndexOf("");
	cout << index << endl;

	index = noString.LastIndexOf("ksdf");
	cout << index << endl;
	cin.get();
}
int main() {
	//charTest();
	//stringClassTest();
	indexTest();
}
