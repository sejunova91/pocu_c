#include "Lab6.h"
#include <limits.h>
#include <map>

namespace lab6
{
	int Sum(const std::vector<int>& v)
	{
		int sum = 0;
		for (std::vector<int>::const_iterator iter = v.begin(); iter != v.end(); ++iter)
		{
			sum += *iter;
		}
		return sum;
	}

	int Min(const std::vector<int>& v)
	{
		int min = INT_MAX;
		for (std::vector<int>::const_iterator iter = v.begin(); iter != v.end(); ++iter)
		{
			if (*iter < min)
			{
				min = *iter;
			}
		}
		return min;
	}

	int Max(const std::vector<int>& v)
	{
		int max = INT_MIN;
		for (std::vector<int>::const_iterator iter = v.begin(); iter != v.end(); ++iter)
		{
			if (*iter > max)
			{
				max = *iter;
			}
		}
		return max;
	}

	float Average(const std::vector<int>& v)
	{
		if (v.size() == 0)
		{
			return 0.0f;
		}
		float sum = static_cast<float>(Sum(v));
		float avg = sum / v.size();
		return avg;
	}

	int NumberWithMaxOccurrence(const std::vector<int>& v)
	{
		if (v.size() == 0)
		{
			return 0;
		}

		int curMaxValueIndex = 0;
		int curMaxValue = 0;
		std::map<int, int> occuranceMap;
		for (size_t i = 0; i < v.size(); ++i)
		{
			std::map<int, int>::iterator mapIter = occuranceMap.find(v[i]);
			if (mapIter == occuranceMap.end())
			{
				occuranceMap.insert(std::pair<int, int>(v[i], 0));
			}
			else
			{
				occuranceMap[v[i]] += 1;
			}
			if (occuranceMap[v[i]] > curMaxValue)
			{
				curMaxValue = occuranceMap[v[i]];
				curMaxValueIndex = v[i];
			}
		}

		return curMaxValueIndex;
	}

	void SortDescending(std::vector<int>& v)
	{
		for (size_t i = 0; i < v.size() - 1; ++i)
		{
			for (size_t j = i + 1; j < v.size(); j++)
			{
				if (v[i] < v[j])
				{
					int temp = v[j];
					v[j] = v[i];
					v[i] = temp;
				}
			}
		}
	}

}