#include "Lab2.h"
#include <string>
#include <iomanip>
#include <float.h>

namespace lab2
{
	void PrintIntegers(std::istream& in, std::ostream& out)
	{
		out << "         oct        dec      hex" << std::endl;
		out << "------------ ---------- --------" << std::endl;
		int number;
		std::string trash;

		while (!(in.eof()))
		{
			in >> number;
			if (!in.fail())
			{
				out << std::setw(12) << std::oct << number;
				out << std::setw(11) << std::dec << number;
				out << std::setw(9) << std::hex << std::uppercase << number << std::endl;
			}
			else
			{
				in.clear();
				in >> trash;
			}
		}
	}

	void PrintMaxFloat(std::istream& in, std::ostream& out)
	{
		float number;
		float max = -FLT_MAX;
		std::string trash;

		while (!(in.eof()))
		{
			in >> number;
			if (!in.fail())
			{
				if (number > max)
				{
					max = number;
				}
				out << "     " << std::setw(15)
					<< std::internal << std::showpos
					<< std::setprecision(3) << std::fixed << number << std::endl;
			}
			else
			{
				in.clear();
				in >> trash;
			}
		}
		out << "max: " << std::setw(15)
			<< std::internal << std::showpos
			<< std::setprecision(3) << std::fixed << max << std::endl;
	}
}
