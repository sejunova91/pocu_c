#pragma once

#include <iostream>
#include <vector>
#include <map>
#include <set>

namespace lab7
{
	template <typename K, class V>
	std::map<K, V> ConvertVectorsToMap(const std::vector<K>& keys, const std::vector<V>& values)
	{
		size_t mapLength = (keys.size() > values.size()) ? values.size() : keys.size();
		std::map<K, V> m;
		for (size_t i = 0; i < mapLength; i++)
		{
			if (m.find(keys[i]) == m.end())
			{
				m[keys[i]] = values[i];
			}
			else
			{
				continue;
			}
		}
		return m;
	}

	template <typename K, class V>
	std::vector<K> GetKeys(const std::map<K, V>& m)
	{
		std::vector<K> v;
		v.reserve(m.size());

		for (typename std::map<K, V>::const_iterator iter = m.begin(); iter != m.end(); iter++)
		{
			v.push_back(iter->first);
		}
		return v;
	}

	template <typename K, class V>
	std::vector<V> GetValues(const std::map<K, V>& m)
	{
		std::vector<V> v;
		v.reserve(m.size());
		for (typename std::map<K, V>::const_iterator iter = m.begin(); iter != m.end(); iter++)
		{
			v.push_back(iter->second);
		}
		return v;
	}

	template <typename T>
	std::vector<T> Reverse(const std::vector<T>& v)
	{
		std::vector<T> rv;
		rv.reserve(v.size());
		for (typename std::vector<T>::const_reverse_iterator iter = v.rbegin(); iter != v.rend(); iter++)
		{
			rv.push_back(*iter);
		}
		return rv;
	}
}

template <typename T>
std::vector<T> operator+(const std::vector<T>& v1, const std::vector<T>& v2)
{
	std::set<T> s;
	std::vector<T> combined;
	for (size_t i = 0; i < v1.size(); i++)
	{
		if (s.find(v1[i]) == s.end())
		{
			s.insert(v1[i]);
			combined.push_back(v1[i]);
		}
	}

	for (size_t i = 0; i < v2.size(); i++)
	{
		if (s.find(v2[i]) == s.end())
		{
			s.insert(v2[i]);
			combined.push_back(v2[i]);
		}
	}
	return combined;
}

template <typename K, class V>
std::map<K, V> operator+(const std::map<K, V>& m1, const std::map<K, V>& m2)
{
	std::map<K, V> combined;
	for (typename std::map<K, V>::const_iterator iter = m1.begin(); iter != m1.end(); iter++)
	{
		combined[iter->first] = iter->second;
	}

	for (typename std::map<K, V>::const_iterator iter = m2.begin(); iter != m2.end(); iter++)
	{
		if (combined.find(iter->first) == combined.end())
		{
			combined[iter->first] = iter->second;
		}
	}

	return combined;
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
{
	for (size_t i = 0; i < v.size(); i++)
	{
		os << v[i];
		if (i != v.size() - 1)
		{
			os << ", ";
		}
	}
	return os;
}

template <typename K, class V>
std::ostream& operator<<(std::ostream& os, const std::map<K, V>& m)
{
	for (typename std::map<K, V>::const_iterator iter = m.begin(); iter != m.end(); iter++)
	{
		os << "{ " << iter->first << ", " << iter->second << " }" << std::endl;
	}
	return os;
}