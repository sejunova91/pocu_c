#pragma once
#include <cstdint>
#include <cstring>
#include <cmath>

namespace lab8
{
	template<size_t N>
	class FixedVector<bool, N>
	{
	public:
		FixedVector();
		bool Add(const bool t);
		bool Remove(const bool t);
		bool Get(const unsigned int index) const;
		bool operator[](const unsigned int index);
		int GetIndex(const bool t) const;
		size_t GetSize() const;
		size_t GetCapacity() const;
	private:
		size_t mSize;
		enum { INTEGER_BIT = 32 };
		uint32_t mArray[(N / INTEGER_BIT) + 1];
	};

	template<size_t N> FixedVector<bool, N>::FixedVector()
		: mSize(0)
	{
		std::memset(mArray, 0, sizeof(mArray));
	}

	template<size_t N>
	bool FixedVector<bool, N>::Add(const bool t)
	{
		if (mSize >= N)
		{
			return false;
		}
		unsigned int index = mSize / INTEGER_BIT;
		unsigned int bitMove = mSize % INTEGER_BIT;

		if (t == true)
		{
			mArray[index] |= (1 << bitMove);
		}
		mSize++;
		return true;
	}

	template<size_t N>
	bool FixedVector<bool, N>::Remove(const bool t)
	{
		const unsigned int ARR_SIZE = (N / INTEGER_BIT) + 1;

		for (size_t i = 0; i < ARR_SIZE; i++)
		{
			if (t == true && mArray[i] == 0)
			{
				continue;
			}
			for (size_t j = 0; j < INTEGER_BIT; j++)
			{
				if ((i * INTEGER_BIT) + (j + 1) > mSize)
				{
					return false;
				}
				const uint32_t oneBitMoved = 1 << j;
				if (t == ((oneBitMoved & mArray[i]) == oneBitMoved))
				{
					uint32_t left = (j == (INTEGER_BIT - 1)) ? 0 : (mArray[i] >> (j + 1)) << j;
					uint32_t right = (j == 0) ? 0 : (mArray[i] << (INTEGER_BIT - j)) >> (INTEGER_BIT - j);
					mArray[i] = left + right;

					for (size_t k = i + 1; k < ARR_SIZE; k++)
					{
						int firstBitOfNextArr = ((1 & mArray[k]) == 1) ? 1 : 0;
						mArray[k] = (mArray[k] >> 1);
						mArray[k - 1] = ((firstBitOfNextArr << (INTEGER_BIT - 1)) | mArray[k - 1]);
					}
					mSize--;
					return true;
				}
			}
		}
		return false;
	}

	template<size_t N>
	bool FixedVector<bool, N>::Get(const unsigned int index) const
	{
		unsigned int arrIndex = index / INTEGER_BIT;
		unsigned int bitIndex = index % INTEGER_BIT;
		const uint32_t oneBitMoved = 1 << bitIndex;
		return (oneBitMoved & mArray[arrIndex]) == oneBitMoved;
	}

	template<size_t N>
	bool FixedVector<bool, N>::operator[](const unsigned int index)
	{
		unsigned int arrIndex = index / INTEGER_BIT;
		unsigned int bitIndex = index % INTEGER_BIT;
		const uint32_t oneBitMoved = 1 << bitIndex;
		return (oneBitMoved & mArray[arrIndex]) == oneBitMoved;
	}

	template<size_t N>
	int FixedVector<bool, N>::GetIndex(const bool t) const
	{
		const unsigned int ARR_SIZE = (N / INTEGER_BIT) + 1;

		for (size_t i = 0; i < ARR_SIZE; i++)
		{
			for (size_t j = 0; j < INTEGER_BIT; j++)
			{
				if ((i * INTEGER_BIT) + (j + 1) > mSize)
				{
					return -1;
				}
				const uint32_t oneBitMoved = 1 << j;
				if (t == ((oneBitMoved & mArray[i]) == oneBitMoved))
				{
					return i * INTEGER_BIT + j;
				}
			}
		}
		return -1;
	}

	template<size_t N>
	size_t FixedVector<bool, N>::GetSize() const
	{
		return mSize;
	}

	template<size_t N>
	size_t FixedVector<bool, N>::GetCapacity() const
	{
		return N;
	}
}