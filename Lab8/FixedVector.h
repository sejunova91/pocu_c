#pragma once

namespace lab8
{
	template<typename T, size_t N>
	class FixedVector
	{
	public:
		FixedVector();
		bool Add(const T t);
		bool Remove(const T t);
		const T& Get(const unsigned int index) const;
		T& operator[](const unsigned int index);
		int GetIndex(const T t) const;
		size_t GetSize() const;
		size_t GetCapacity() const;
	private:
		size_t mSize;
		T mArray[N];
	};

	template<typename T, size_t N> FixedVector<T, N>::FixedVector()
		: mSize(0)
	{
	}

	template<typename T, size_t N>
	bool FixedVector<T, N>::Add(const T t)
	{
		if (mSize < N)
		{
			mArray[mSize] = t;
			mSize++;
			return true;
		}
		return false;
	}

	template<typename T, size_t N>
	bool FixedVector<T, N>::Remove(const T t)
	{
		size_t foundIndex = -1;
		for (size_t i = 0; i < mSize; i++)
		{
			if (mArray[i] == t)
			{
				foundIndex = i;
				break;
			}
		}
		if (foundIndex == -1)
		{
			return false;
		}

		for (size_t j = foundIndex + 1; j < mSize; j++)
		{
			mArray[j - 1] = mArray[j];
		}
		mSize--;
		return true;
	}

	template<typename T, size_t N>
	const T& FixedVector<T, N>::Get(const unsigned int index) const
	{
		return mArray[index];
	}

	template<typename T, size_t N>
	T& FixedVector<T, N>::operator[](const unsigned int index)
	{
		return mArray[index];
	}

	template<typename T, size_t N>
	int FixedVector<T, N>::GetIndex(const T t) const
	{
		for (size_t i = 0; i < mSize; i++)
		{
			if (mArray[i] == t)
			{
				return i;
			}
		}
		return -1;
	}

	template<typename T, size_t N>
	size_t FixedVector<T, N>::GetSize() const
	{
		return mSize;
	}

	template<typename T, size_t N>
	size_t FixedVector<T, N>::GetCapacity() const
	{
		return N;
	}
}