#include "Motorcycle.h"

namespace assignment2
{
	const unsigned int MAX_MOTORCYCLE_PASSENGER = 2;
	Motorcycle::Motorcycle()
		: Vehicle(MAX_MOTORCYCLE_PASSENGER)
	{
	}

	Motorcycle::~Motorcycle()
	{
	}

	unsigned int Motorcycle::GetMaxSpeed()
	{
		return this->GetDriveSpeed();
	}

	unsigned int Motorcycle::GetDriveSpeed()
	{
		double speedByWeight = pow(-(static_cast<double>(mWeight) / 15), 3) + (2 * mWeight) + 400;
		if (speedByWeight > 0)
		{
			return static_cast<unsigned int>(round(speedByWeight));
		}
		return 0;
	}

	void Motorcycle::Travel()
	{
		switch (mTravelPatternCount)
		{
		case 0:
			mTotalTravelDistance += this->GetMaxSpeed();
			break;
		case 1:
			mTotalTravelDistance += this->GetMaxSpeed();
			break;
		case 2:
			mTotalTravelDistance += this->GetMaxSpeed();
			break;
		case 3:
			mTotalTravelDistance += this->GetMaxSpeed();
			break;
		case 4:
			mTotalTravelDistance += this->GetMaxSpeed();
			break;
		default:
			break;
		}
		if (mTravelPatternCount % 6 == 5)
		{
			mTravelPatternCount = 0;
		}
		else
		{
			mTravelPatternCount++;
		}
	}
}