#include "Boatplane.h"
#include "Constants.h"

namespace assignment2
{
	Boatplane::Boatplane(unsigned int maxPassengersCount)
		: Vehicle(maxPassengersCount)
	{
	}

	Boatplane::Boatplane(Boatplane& other)
		: Vehicle(other.mPassengerCountMax)
	{
		mPassengerArrCount = other.mPassengerArrCount;
		mWeight = other.mWeight;
		mTotalTravelDistance = 0;
		mTravelPatternCount = 0;

		for (unsigned int i = 0; i < other.mPassengerArrCount; i++)
		{
			mPassengerArr[i] = other.mPassengerArr[i];
		}
		other.EmptyVehicle();
	}

	Boatplane::~Boatplane()
	{
	}

	unsigned int Boatplane::GetMaxSpeed()
	{
		if (this->GetFlySpeed() > this->GetSailSpeed())
		{
			return this->GetFlySpeed();
		}
		return this->GetSailSpeed();
	}

	unsigned int Boatplane::GetFlySpeed()
	{
		double speed = 150 * pow(Euler_Constant, ((static_cast<double>(-(static_cast<int>(mWeight)) + 500)) / 300));
		return static_cast<unsigned int>(round(speed));
	}

	unsigned int Boatplane::GetSailSpeed()
	{
		float speedByWeight = 800 - (1.7f * mWeight);
		if (speedByWeight > 20)
		{
			return static_cast<unsigned int>(round(speedByWeight));
		}
		return 20;
	}

	void Boatplane::Travel()
	{
		switch (mTravelPatternCount)
		{
		case 0:
			mTotalTravelDistance += this->GetMaxSpeed();
			break;
		default:
			break;
		}
		if (mTravelPatternCount % 4 == 3)
		{
			mTravelPatternCount = 0;
		}
		else
		{
			mTravelPatternCount++;
		}
	}
}