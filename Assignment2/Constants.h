#pragma once

#include <cmath>

namespace assignment2
{
	static const double Euler_Constant = std::exp(1.0);
}