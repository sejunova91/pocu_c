#include "UBoat.h"

namespace assignment2
{
	const unsigned int MAX_UBOAT_PASSENGER = 50;
	UBoat::UBoat()
		: Vehicle(MAX_UBOAT_PASSENGER)
	{
	}

	UBoat::~UBoat()
	{
	}

	unsigned int UBoat::GetMaxSpeed()
	{
		if (this->GetDiveSpeed() > this->GetSailSpeed())
		{
			return this->GetDiveSpeed();
		}
		return this->GetSailSpeed();
	}

	unsigned int UBoat::GetSailSpeed()
	{
		double speedByWeight = 550 - (static_cast<double>(mWeight) / 10);
		if (speedByWeight > 200)
		{
			return static_cast<unsigned int>(round(speedByWeight));
		}
		return 200;
	}

	unsigned int UBoat::GetDiveSpeed()
	{
		double speed = 500 * log(static_cast<double>((mWeight + 150)) / 150) + 30;
		return static_cast<unsigned int>(round(speed));
	}

	void UBoat::Travel()
	{
		switch (mTravelPatternCount)
		{
		case 0:
			mTotalTravelDistance += this->GetMaxSpeed();
			break;
		case 1:
			mTotalTravelDistance += this->GetMaxSpeed();
			break;
		default:
			break;
		}
		if (mTravelPatternCount % 6 == 5)
		{
			mTravelPatternCount = 0;
		}
		else
		{
			mTravelPatternCount++;
		}
	}
}