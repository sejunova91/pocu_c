#include "DeusExMachina.h"

namespace assignment2
{
	DeusExMachina* DeusExMachina::mInstance = nullptr;
	DeusExMachina* DeusExMachina::GetInstance()
	{
		if (mInstance == nullptr)
		{
			mInstance = new DeusExMachina();
			mInstance->mVehicleArrCount = 0;
		}
		return mInstance;
	}

	DeusExMachina::~DeusExMachina()
	{
		for (unsigned int i = 0; i < mVehicleArrCount; ++i)
		{
			delete mVehicleArr[i];
		}
	}

	void DeusExMachina::Travel() const
	{
		for (unsigned int i = 0; i < mVehicleArrCount; ++i)
		{
			mVehicleArr[i]->Travel();
		}
	}

	bool DeusExMachina::AddVehicle(Vehicle* vehicle)
	{
		if (mVehicleArrCount < VEHICLE_MAX_COUNT && vehicle != nullptr)
		{
			mVehicleArr[mVehicleArrCount] = vehicle;
			mVehicleArrCount++;
			return true;
		}
		return false;
	}

	Vehicle* DeusExMachina::GetVehicle(unsigned int i)
	{
		return mVehicleArr[i];
	}

	bool DeusExMachina::RemoveVehicle(unsigned int i)
	{
		if (i >= mVehicleArrCount)
		{
			return false;
		}
		delete mVehicleArr[i];
		mVehicleArrCount--;
		for (unsigned int j = i; j < mVehicleArrCount; ++j)
		{
			mVehicleArr[j] = mVehicleArr[j + 1];
		}
		return true;
	}

	const Vehicle* DeusExMachina::GetFurthestTravelled() const
	{
		if (mVehicleArrCount == 0)
		{
			return NULL;
		}
		unsigned int furthestDistance = 0;
		unsigned int furthestTravelledVehicleIndex = 0;
		for (unsigned int i = 0; i < mVehicleArrCount; ++i)
		{
			if (mVehicleArr[i]->GetTravelledDistance() > furthestDistance)
			{
				furthestDistance = mVehicleArr[i]->GetTravelledDistance();
				furthestTravelledVehicleIndex = i;
			}
		}
		if (furthestDistance == 0)
		{
			return mVehicleArr[0];
		}
		return mVehicleArr[furthestTravelledVehicleIndex];
	}
}