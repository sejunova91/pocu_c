#pragma once

#include "Person.h"
#include <cmath>

namespace assignment2
{
	const unsigned int VEHICLE_MAX_PASSENGER_COUNT = 100;
	class Vehicle
	{
	public:
		Vehicle(unsigned int maxPassengersCount);
		virtual ~Vehicle();

		virtual unsigned int GetMaxSpeed() = 0;
		virtual void Travel() = 0;
		unsigned int GetTravelledDistance();

		bool AddPassenger(const Person* person);
		bool RemovePassenger(unsigned int i);
		const Person* GetPassenger(unsigned int i) const;
		unsigned int GetPassengersCount() const;
		unsigned int GetMaxPassengersCount() const;
		void EmptyVehicle();

	protected:
		const Person* mPassengerArr[VEHICLE_MAX_PASSENGER_COUNT];
		unsigned int mPassengerArrCount;
		const unsigned int mPassengerCountMax;
		unsigned int mWeight;
		unsigned int mTotalTravelDistance;
		unsigned int mTravelPatternCount;
	};
}