#pragma once

#include "Vehicle.h"

namespace assignment2
{
	const unsigned int VEHICLE_MAX_COUNT = 10;
	class DeusExMachina
	{
	public:
		static DeusExMachina* GetInstance();
		~DeusExMachina();
		void Travel() const;
		bool AddVehicle(Vehicle* vehicle);
		Vehicle* GetVehicle(unsigned int i);
		bool RemoveVehicle(unsigned int i);
		const Vehicle* GetFurthestTravelled() const;

	private:
		DeusExMachina() {};
		DeusExMachina(const DeusExMachina& other);

		static DeusExMachina* mInstance;

		Vehicle* mVehicleArr[VEHICLE_MAX_COUNT];
		unsigned int mVehicleArrCount;
	};
}
