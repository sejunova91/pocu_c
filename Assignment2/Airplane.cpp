#include "Airplane.h"
#include "Boat.h"
#include "Boatplane.h"
#include "Constants.h"

namespace assignment2
{
	Airplane::Airplane(unsigned int maxPassengersCount)
		: Vehicle(maxPassengersCount)
	{
	}

	Airplane::~Airplane()
	{
	}

	unsigned int Airplane::GetMaxSpeed()
	{
		if (this->GetFlySpeed() > this->GetDriveSpeed())
		{
			return this->GetFlySpeed();
		}
		return this->GetDriveSpeed();
	}

	unsigned int Airplane::GetFlySpeed()
	{
		double speed = 200 * pow(Euler_Constant, ((static_cast<double>(-(static_cast<int>(mWeight)) + 800)) / 500));
		return static_cast<unsigned int>(round(speed));
	}

	unsigned int Airplane::GetDriveSpeed()
	{
		double speed = 4 * pow(Euler_Constant, ((static_cast<double>(-(static_cast<int>(mWeight)) + 400)) / 70));
		return static_cast<unsigned int>(round(speed));
	}

	void Airplane::Travel()
	{
		switch (mTravelPatternCount)
		{
		case 0:
			mTotalTravelDistance += this->GetMaxSpeed();
			break;
		default:
			break;
		}
		if (mTravelPatternCount % 4 == 3)
		{
			mTravelPatternCount = 0;
		}
		else
		{
			mTravelPatternCount++;
		}
	}

	Boatplane Airplane::operator+(Boat& boat)
	{
		unsigned int boatPlaneMaxPassengerCount = mPassengerCountMax + boat.GetMaxPassengersCount();
		Boatplane bp(boatPlaneMaxPassengerCount);
		for (unsigned int i = 0; i < mPassengerArrCount; ++i)
		{
			bp.AddPassenger(mPassengerArr[i]);
		}
		for (unsigned int j = 0; j < boat.GetPassengersCount(); ++j)
		{
			bp.AddPassenger(boat.GetPassenger(j));
		}
		this->EmptyVehicle();
		boat.EmptyVehicle();

		return bp;
	}
}