#include "Vehicle.h"

namespace assignment2
{
	Vehicle::Vehicle(unsigned int maxPassengersCount)
		: mPassengerArrCount(0)
		, mPassengerCountMax(maxPassengersCount)
		, mWeight(0)
		, mTotalTravelDistance(0)
		, mTravelPatternCount(0)
	{
	}

	Vehicle::~Vehicle()
	{
		for (unsigned int i = 0; i < mPassengerArrCount; ++i)
		{
			delete mPassengerArr[i];
		}
	}

	bool Vehicle::AddPassenger(const Person* person)
	{
		if (mPassengerArrCount < mPassengerCountMax && person != nullptr)
		{
			mPassengerArr[mPassengerArrCount] = person;
			mPassengerArrCount++;
			mWeight += person->GetWeight();
			return true;
		}
		return false;
	}

	bool Vehicle::RemovePassenger(unsigned int i)
	{
		if (i >= mPassengerArrCount)
		{
			return false;
		}
		mWeight -= mPassengerArr[i]->GetWeight();
		delete mPassengerArr[i];
		mPassengerArrCount--;
		for (unsigned int j = i; j < mPassengerArrCount; ++j)
		{
			mPassengerArr[j] = mPassengerArr[j + 1];
		}
		return true;
	}

	unsigned int Vehicle::GetPassengersCount() const
	{
		return mPassengerArrCount;
	}

	unsigned int Vehicle::GetMaxPassengersCount() const
	{
		return mPassengerCountMax;
	}

	const Person* Vehicle::GetPassenger(unsigned int i) const
	{
		if (i >= mPassengerArrCount)
		{
			return NULL;
		}
		return mPassengerArr[i];
	}

	void Vehicle::EmptyVehicle()
	{
		for (unsigned int i = 0; i < mPassengerArrCount; ++i)
		{
			mPassengerArr[i] = nullptr;
		}
		mWeight = 0;
		mPassengerArrCount = 0;
	}

	unsigned int Vehicle::GetTravelledDistance()
	{
		return mTotalTravelDistance;
	}
}