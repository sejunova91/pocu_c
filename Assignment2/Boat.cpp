#include "Airplane.h"
#include "Boat.h"
#include "Boatplane.h"

namespace assignment2
{
	Boat::Boat(unsigned int maxPassengersCount)
		: Vehicle(maxPassengersCount)
	{
	}

	Boat::~Boat()
	{
	}

	unsigned int Boat::GetMaxSpeed()
	{
		return this->GetSailSpeed();
	}

	unsigned int Boat::GetSailSpeed()
	{
		int speedByWeight = 800 - (10 * mWeight);
		if (speedByWeight > 20)
		{
			return static_cast<unsigned int>(speedByWeight);
		}
		return 20;
	}

	void Boat::Travel()
	{
		switch (mTravelPatternCount)
		{
		case 0:
			mTotalTravelDistance += this->GetMaxSpeed();
			break;
		case 1:
			mTotalTravelDistance += this->GetMaxSpeed();
			break;
		default:
			break;
		}
		if (mTravelPatternCount % 3 == 2)
		{
			mTravelPatternCount = 0;
		}
		else
		{
			mTravelPatternCount++;
		}
	}

	Boatplane Boat::operator+(Airplane& plane)
	{
		unsigned int boatPlaneMaxPassengerCount = mPassengerCountMax + plane.GetMaxPassengersCount();
		Boatplane bp(boatPlaneMaxPassengerCount);
		for (unsigned int j = 0; j < plane.GetPassengersCount(); ++j)
		{
			bp.AddPassenger(plane.GetPassenger(j));
		}
		for (unsigned int i = 0; i < mPassengerArrCount; ++i)
		{
			bp.AddPassenger(mPassengerArr[i]);
		}
		this->EmptyVehicle();
		plane.EmptyVehicle();

		return bp;
	}
}