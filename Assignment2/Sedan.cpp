#include "Sedan.h"
#include "Trailer.h"

namespace assignment2
{
	const unsigned int MAX_SEDAN_PASSENGER = 4;
	Sedan::Sedan()
		: Vehicle(MAX_SEDAN_PASSENGER)
		, mTrailer(nullptr)
	{
	}

	Sedan::~Sedan()
	{
		if (mTrailer != nullptr)
		{
			delete mTrailer;
		}
	}

	bool Sedan::AddTrailer(const Trailer* trailer)
	{
		if (mTrailer != nullptr)
		{
			return false;
		}
		mTrailer = trailer;
		mWeight += trailer->GetWeight();
		return true;
	}

	bool Sedan::RemoveTrailer()
	{
		if (mTrailer == nullptr)
		{
			return false;
		}
		mWeight -= mTrailer->GetWeight();
		delete mTrailer;
		mTrailer = nullptr;
		return true;
	}

	unsigned int Sedan::GetMaxSpeed()
	{
		return this->GetDriveSpeed();
	}

	unsigned int Sedan::GetDriveSpeed()
	{
		if (mWeight <= 80)
		{
			return 480;
		}
		if (mWeight <= 160)
		{
			return 458;
		}
		if (mWeight <= 260)
		{
			return 400;
		}
		if (mWeight <= 350)
		{
			return 380;
		}
		return 300;
	}

	void Sedan::Travel()
	{
		switch (mTravelPatternCount)
		{
		case 0:
			mTotalTravelDistance += this->GetMaxSpeed();
			break;
		case 1:
			mTotalTravelDistance += this->GetMaxSpeed();
			break;
		case 2:
			mTotalTravelDistance += this->GetMaxSpeed();
			break;
		case 3:
			mTotalTravelDistance += this->GetMaxSpeed();
			break;
		case 4:
			mTotalTravelDistance += this->GetMaxSpeed();
			break;
		default:
			break;
		}
		if (mTrailer == nullptr)
		{
			if (mTravelPatternCount % 6 == 5)
			{
				mTravelPatternCount = 0;
			}
			else
			{
				mTravelPatternCount++;
			}
		}
		else
		{
			if (mTravelPatternCount % 7 == 6)
			{
				mTravelPatternCount = 0;
			}
			else
			{
				mTravelPatternCount++;
			}
		}
	}
}