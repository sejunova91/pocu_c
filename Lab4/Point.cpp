#include "Point.h"

namespace lab4
{
	Point::Point(float x, float y)
		: mX(x)
		, mY(y)
	{
	}

	Point::~Point()
	{
	}

	Point Point::operator+(const Point& other) const
	{
		float x;
		float y;

		x = mX + other.mX;
		y = mY + other.mY;
		return Point(x, y);
	}

	Point Point::operator-(const Point& other) const
	{
		float x;
		float y;

		x = mX - other.mX;
		y = mY - other.mY;
		return Point(x, y);
	}

	float Point::Dot(const Point& other) const
	{
		float dot;
		dot = (mX * other.mX) + (mY * other.mY);
		return dot;
	}

	Point Point::operator*(float operand) const
	{
		float x;
		float y;

		x = mX * operand;
		y = mY * operand;
		return Point(x, y);
	}

	Point operator*(float operand, const Point& other)
	{
		float x;
		float y;

		x = other.mX * operand;
		y = other.mY * operand;
		return Point(x, y);
	}

	float Point::GetX() const
	{
		return mX;
	}

	float Point::GetY() const
	{
		return mY;
	}

	void Point::SetX(float x)
	{
		mX = x;
	}

	void Point::SetY(float y)
	{
		mY = y;
	}
}