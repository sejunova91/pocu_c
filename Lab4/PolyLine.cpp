#include <cstring>
#include <cmath>
#include "PolyLine.h"

namespace lab4
{
	PolyLine::PolyLine()
		: mLength(0)
	{
	}

	PolyLine::PolyLine(const PolyLine& other)
		: mLength(other.mLength)
	{
		for (unsigned int i = 0; i < other.mLength; ++i)
		{
			Point* newPointPtr = other.mPointArr[i];
			Point* newPoint = new Point(*newPointPtr);
			mPointArr[i] = newPoint;
		}
	}

	PolyLine::~PolyLine()
	{
		for (unsigned int i = 0; i < mLength; ++i)
		{
			delete mPointArr[i];
		}
	}

	bool PolyLine::AddPoint(float x, float y)
	{
		if (mLength < MAX_POINT_ARR_SIZE)
		{
			Point* newPoint = new Point(x, y);
			mPointArr[mLength] = newPoint;
			mLength++;
			return true;
		}
		else
		{
			return false;
		}
	}

	bool PolyLine::AddPoint(const Point* point)
	{
		if (mLength < MAX_POINT_ARR_SIZE && point != nullptr)
		{
			mPointArr[mLength] = const_cast<Point*>(point);
			mLength++;
			return true;
		}
		else
		{
			return false;
		}
	}

	bool PolyLine::RemovePoint(unsigned int i)
	{
		if (i >= mLength)
		{
			return false;
		}
		delete mPointArr[i];
		mLength--;
		for (unsigned int j = i; j < mLength; ++j)
		{
			mPointArr[j] = mPointArr[j + 1];
		}
		return true;
	}

	bool PolyLine::TryGetMinBoundingRectangle(Point* outMin, Point* outMax) const
	{
		if (mLength <= 2)
		{
			return false;
		}
		bool bXFound = false;
		bool bYFound = false;
		for (unsigned int j = 0; j < mLength; ++j)
		{
			if (mPointArr[j]->GetX() != 0.0f)
			{
				bXFound = true;
			}
			if (mPointArr[j]->GetY() != 0.0f)
			{
				bYFound = true;
			}
		}
		if (!bXFound or !bYFound)
		{
			return false;
		}

		float xMin = mPointArr[0]->GetX();
		float xMax = mPointArr[0]->GetX();
		float yMin = mPointArr[0]->GetY();
		float yMax = mPointArr[0]->GetY();

		for (unsigned int i = 1; i < mLength; ++i)
		{
			if (mPointArr[i]->GetX() < xMin)
			{
				xMin = mPointArr[i]->GetX();
			}
			if (mPointArr[i]->GetX() > xMax)
			{
				xMax = mPointArr[i]->GetX();
			}
			if (mPointArr[i]->GetY() < yMin)
			{
				yMin = mPointArr[i]->GetY();
			}
			if (mPointArr[i]->GetY() > yMax)
			{
				yMax = mPointArr[i]->GetY();
			}
		}
		outMin->SetX(xMin);
		outMin->SetY(yMin);
		outMax->SetX(xMax);
		outMax->SetY(yMax);
		return true;
	}

	const Point* PolyLine::operator[](unsigned int i) const
	{
		if (i >= mLength)
		{
			return nullptr;
		}
		return mPointArr[i];
	}
}
