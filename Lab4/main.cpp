#include <cassert>
#include <iostream>
#include "Point.h"
#include "PolyLine.h"
using namespace lab4;
using namespace std;
int main()
{
    PolyLine pl;
    float fx, fy;
    for (int i = 0; i < 9; ++i)
    {
        pl.AddPoint(new Point((float)i, (float)i));
    }
    Point* plastPoint = new Point(9.0f, 9.0f);
    fx = plastPoint->GetX();
    fy = plastPoint->GetY();
    assert(pl.AddPoint(plastPoint) == true);
    assert(plastPoint != nullptr);
    assert((plastPoint->GetX() == fx) && (plastPoint->GetY() == fy));
    assert(pl.RemovePoint(9) == true);
    assert((plastPoint->GetX() != fx) && (plastPoint->GetY() != fy)); //<메모리 엑세스 오류가 발생>하거나 <assert(TRUE)>를 만족해야 한다.
    //assert((plastPoint->GetX() == fx) && (plastPoint->GetY() == fy)); //abort() has been called!!.

    assert(pl.AddPoint(new Point(9.0f, 9.0f)) == true);
    Point* pt = new Point(10.0f, 10.f);
    fx = pt->GetX();
    fy = pt->GetY();
    assert(pl.AddPoint(pt) == false);
    assert(pt != nullptr);
    assert((pt->GetX() == fx) && (pt->GetY() == fy));
}