#pragma once

#include <memory>

namespace lab11
{
	template<typename T>
	class Storage
	{
	public:
		Storage(unsigned int length);
		Storage(unsigned int length, const T& initialValue);
		Storage(Storage& other);
		Storage(Storage&& other);

		Storage& operator=(Storage& other);
		Storage& operator=(Storage&& other);
		bool Update(unsigned int index, const T& data);
		const std::unique_ptr<T[]>& GetData() const;
		unsigned int GetSize() const;
		std::unique_ptr<T[]> mArray;

	private:
		unsigned int mLength;
	};

	template<typename T> Storage<T>::Storage(unsigned int length)
		: mLength(length)
	{
		T initialValue = T();
		mArray = std::make_unique<T[]>(length);
		for (size_t i = 0; i < length; i++)
		{
			mArray[i] = initialValue;
		}
	}

	template<typename T> Storage<T>::Storage(unsigned int length, const T& initialValue)
		: mLength(length)
	{
		mArray = std::make_unique<T[]>(length);
		for (size_t i = 0; i < length; i++)
		{
			mArray[i] = initialValue;
		}
	}

	template<typename T> Storage<T>::Storage(Storage& other)
		: mLength(other.mLength)
	{
		mArray = std::make_unique<T[]>(other.mLength);
		for (size_t i = 0; i < mLength; i++)
		{
			mArray[i] = other.mArray[i];
		}
	}

	template<typename T> Storage<T>::Storage(Storage&& other)
		: mLength(other.mLength)
		, mArray(std::move(other.mArray))
	{
		other.mLength = 0;
		other.mArray = nullptr;
	}

	template<typename T>
	Storage<T>& Storage<T>::operator=(Storage& other)
	{
		mLength = other.mLength;

		if (this != &other)
		{
			mArray = nullptr;
			mArray = std::make_unique<T[]>(mLength);
		}

		for (size_t i = 0; i < mLength; i++)
		{
			mArray[i] = other.mArray[i];
		}
		return *this;
	}

	template<typename T>
	Storage<T>& Storage<T>::operator=(Storage&& other)
	{
		if (this != &other)
		{
			mLength = other.mLength;
			mArray = std::move(other.mArray);
			other.mLength = 0;
			other.mArray = nullptr;
		}
		return *this;
	}

	template<typename T>
	bool Storage<T>::Update(unsigned int index, const T& data)
	{
		if (index >= mLength)
		{
			return false;
		}
		else
		{
			mArray[index] = data;
			return true;
		}
	}

	template<typename T>
	const std::unique_ptr<T[]>& Storage<T>::GetData() const
	{
		return mArray;
	}

	template<typename T>
	unsigned int Storage<T>::GetSize() const
	{
		return mLength;
	}
}