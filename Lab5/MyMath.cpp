#include "MyMath.h"

namespace lab5
{
	unsigned int MyMath::Ceil(double value)
	{
		unsigned int intValue = static_cast<unsigned int>(value);
		if (value == static_cast<double>(intValue))
		{
			return intValue;
		}

		return intValue + 1;
	}

	unsigned int MyMath::Round(double value)
	{
		return static_cast<unsigned int>(value + 0.5);
	}
}
