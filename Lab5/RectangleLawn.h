#pragma once

#include "Lawn.h"
#include "IFenceable.h"

namespace lab5
{
	class RectangleLawn : public Lawn, IFenceable
	{
	public:
		RectangleLawn(unsigned int x, unsigned int y);
		virtual ~RectangleLawn();

		unsigned int GetArea() const;

		unsigned int GetMinimumFencesCount() const;
		unsigned int GetFencePrice(eFenceType fenceType) const;

	private:
		unsigned int mPerimeter;
	};
}
