#include <math.h>
#include "EquilateralTriangleLawn.h"
#include "MyMath.h"

namespace lab5
{
	EquilateralTriangleLawn::EquilateralTriangleLawn(unsigned int x)
	{
		mArea = MyMath::Round(sqrt(3) / 4 * (x * x));
		mPerimeter = 3 * x;
	}

	EquilateralTriangleLawn::~EquilateralTriangleLawn()
	{
	}

	unsigned int EquilateralTriangleLawn::GetArea() const
	{
		return mArea;
	}

	unsigned int EquilateralTriangleLawn::GetMinimumFencesCount() const
	{
		const double FENCE_WIDTH = 0.25;
		double beforeCeiling = mPerimeter / FENCE_WIDTH;
		return MyMath::Ceil(beforeCeiling);
	}
	unsigned int EquilateralTriangleLawn::GetFencePrice(eFenceType fenceType) const
	{
		const double RED_CEDAR_PRICE_PER_METER = 6;
		const double SPRUCE_PRICE_PER_METER = 7;

		double fencePrice;
		unsigned int fenceCount;

		switch (fenceType)
		{
		case RED_CEDAR:
			fencePrice = RED_CEDAR_PRICE_PER_METER;
			break;
		case SPRUCE:
			fencePrice = SPRUCE_PRICE_PER_METER;
		default:
			break;
		}
		fenceCount = this->GetMinimumFencesCount();
		double fencePricePerSquareMeter = fencePrice / 4;
		return static_cast<unsigned int>(fencePricePerSquareMeter * fenceCount);
	}
}
