#pragma once

#include "Lawn.h"
#include "IFenceable.h"

namespace lab5
{
	class EquilateralTriangleLawn : public Lawn, IFenceable
	{
	public:
		EquilateralTriangleLawn(unsigned int x);
		virtual ~EquilateralTriangleLawn();

		unsigned int GetArea() const;

		unsigned int GetMinimumFencesCount() const;
		unsigned int GetFencePrice(eFenceType fenceType) const;

	private:
		unsigned int mPerimeter;
	};
}
