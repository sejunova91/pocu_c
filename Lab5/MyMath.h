#pragma once

namespace lab5
{
	class MyMath
	{
	public:
		static unsigned int Ceil(double value);
		static unsigned int Round(double value);

	private:
		MyMath() {};
	};
}
