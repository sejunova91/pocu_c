#include "Lawn.h"
#include "MyMath.h"

namespace lab5
{
	Lawn::Lawn()
	{
	}

	Lawn::~Lawn()
	{
	}

	unsigned int Lawn::GetGrassPrice(eGrassType grassType) const
	{
		const double BERMUDA_PRICE_PER_SQURE_METER = 8;
		const double BAHIA_PRICE_PER_SQURE_METER = 5;
		const double BENT_GRASS_PRICE_PER_SQURE_METER = 3;
		const double PERENNIAL_RYEGRASS_PRICE_PER_SQURE_METER = 2.5;
		const double ST_AUGUSTINE_PRICE_PER_SQURE_METER = 4.5;

		double grassPrice;
		switch (grassType)
		{
		case BERMUDA:
			grassPrice = BERMUDA_PRICE_PER_SQURE_METER;
			break;
		case BAHIA:
			grassPrice = BAHIA_PRICE_PER_SQURE_METER;
			break;
		case BENTGRASS:
			grassPrice = BENT_GRASS_PRICE_PER_SQURE_METER;
			break;
		case PERENNIAL_RYEGRASS:
			grassPrice = PERENNIAL_RYEGRASS_PRICE_PER_SQURE_METER;
			break;
		case ST_AUGUSTINE:
			grassPrice = ST_AUGUSTINE_PRICE_PER_SQURE_METER;
			break;
		default:
			break;
		}
		return MyMath::Round(mArea * grassPrice);
	}

	unsigned int Lawn::GetMinimumSodRollsCount() const
	{
		const double SOD_ROLL_AREA = 0.3;
		double minimumSodRollsCountBeforeCeiling = mArea / SOD_ROLL_AREA;
		return MyMath::Ceil(minimumSodRollsCountBeforeCeiling);
	}
}