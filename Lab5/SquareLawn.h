#pragma once

#include "Lawn.h"
#include "RectangleLawn.h"
#include "IFenceable.h"

namespace lab5
{
	class SquareLawn : public RectangleLawn
	{
	public:
		SquareLawn(unsigned int x);
		virtual ~SquareLawn();
	};
}
