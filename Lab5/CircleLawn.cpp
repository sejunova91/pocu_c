#include "CircleLawn.h"
#include "MyMath.h"

namespace lab5
{
	CircleLawn::CircleLawn(unsigned int x)
	{
		const double PI = 3.14;
		mArea = MyMath::Round(x * x * PI);
	}

	CircleLawn::~CircleLawn()
	{
	}

	unsigned int CircleLawn::GetArea() const
	{
		return mArea;
	}
}
