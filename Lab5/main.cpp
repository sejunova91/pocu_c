#include <cassert>
#include <iostream>
#include "RectangleLawn.h"
#include "EquilateralTriangleLawn.h"
#include "SquareLawn.h"
#include "CircleLawn.h"
#include "Lawn.h"

using namespace lab5;
using namespace std;

void TestRectangleLawn()
{
	Lawn* lawn = new RectangleLawn(10, 20);
	unsigned int area = lawn->GetArea();
	unsigned int grassPrice = lawn->GetGrassPrice(BERMUDA);
	unsigned int sodRollsCount = lawn->GetMinimumSodRollsCount();

	assert(area == 200);
	assert(grassPrice == 1600);
	assert(sodRollsCount == 667);

	RectangleLawn* rectangleLawn = (RectangleLawn*)lawn;

	unsigned int fencePrice = rectangleLawn->GetFencePrice(RED_CEDAR);
	unsigned int fencesCount = rectangleLawn->GetMinimumFencesCount();

	assert(fencePrice == 360);
	assert(fencesCount == 240);
	assert(fencesCount == 240);
}

void TestSquareLawn()
{
	Lawn* lawn = new SquareLawn(10);
	unsigned int area = lawn->GetArea();
	unsigned int grassPrice = lawn->GetGrassPrice(BERMUDA);
	unsigned int sodRollsCount = lawn->GetMinimumSodRollsCount();

	assert(area == 100);
	assert(grassPrice == 800);
	assert(sodRollsCount == 334);

	SquareLawn* rectangleLawn = (SquareLawn*)lawn;

	unsigned int fencePrice = rectangleLawn->GetFencePrice(RED_CEDAR);
	unsigned int fencesCount = rectangleLawn->GetMinimumFencesCount();

	assert(fencePrice == 240);
	assert(fencesCount == 160);
}

void TestEquilateralTriangleLawn()
{
	Lawn* lawn = new EquilateralTriangleLawn(5);
	unsigned int area = lawn->GetArea();
	unsigned int grassPrice = lawn->GetGrassPrice(BERMUDA);
	unsigned int sodRollsCount = lawn->GetMinimumSodRollsCount();

	assert(area == 13);
	assert(grassPrice == 104);
	assert(sodRollsCount == 44);

	EquilateralTriangleLawn* equilateralTriangleLawn = (EquilateralTriangleLawn*)lawn;

	unsigned int fencePrice = equilateralTriangleLawn->GetFencePrice(RED_CEDAR);
	unsigned int fencesCount = equilateralTriangleLawn->GetMinimumFencesCount();

	assert(fencePrice == 90);
	assert(fencesCount == 60);
}

void TestCircleLawn()
{
	Lawn* lawn = new CircleLawn(5);
	unsigned int area = lawn->GetArea();
	unsigned int grassPrice = lawn->GetGrassPrice(BERMUDA);
	unsigned int sodRollsCount = lawn->GetMinimumSodRollsCount();

	assert(area == 79);
	assert(grassPrice == 632);
	assert(sodRollsCount == 264);
}

int main()
{
	TestRectangleLawn();
	TestSquareLawn();
	TestEquilateralTriangleLawn();
	TestCircleLawn();
	return 0;
}