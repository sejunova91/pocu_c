#include "Timesheet.h"
#include <cmath>

namespace lab3
{
	TimeSheet::TimeSheet(const char* name, unsigned int maxEntries)
		: mName(name)
		, mWorkedHoursLength(maxEntries)
		, mCurrentWorkdHoursCount(0)
	{
		mWorkedHours = new unsigned int[maxEntries];
	}

	TimeSheet::TimeSheet(const TimeSheet& other)
		: mName(other.mName)
		, mWorkedHoursLength(other.mWorkedHoursLength)
		, mCurrentWorkdHoursCount(other.mCurrentWorkdHoursCount)
	{
		mWorkedHours = new unsigned int[other.mWorkedHoursLength];
		for (unsigned int i = 0; i < other.mCurrentWorkdHoursCount; i++)
		{
			mWorkedHours[i] = other.mWorkedHours[i];
		}
	}

	TimeSheet::~TimeSheet()
	{
		delete[] mWorkedHours;
	}

	void TimeSheet::AddTime(const int timeInHours)
	{
		if ((1 <= timeInHours && timeInHours <= 10) && (mCurrentWorkdHoursCount < mWorkedHoursLength))
		{
			mWorkedHours[mCurrentWorkdHoursCount] = timeInHours;
			mCurrentWorkdHoursCount++;
		}
	}

	int TimeSheet::GetTimeEntry(unsigned int index) const
	{
		if (index < mCurrentWorkdHoursCount)
		{
			return mWorkedHours[index];
		}
		else
		{
			return -1;
		}
	}

	int TimeSheet::GetTotalTime() const
	{
		unsigned int totalTime = 0;
		for (unsigned int i = 0; i < mCurrentWorkdHoursCount; i++)
		{
			totalTime += mWorkedHours[i];
		}
		return totalTime;
	}

	float TimeSheet::GetAverageTime() const
	{
		if (mCurrentWorkdHoursCount == 0)
		{
			return 0.0f;
		}

		unsigned int totalTime;
		float average;

		totalTime = this->GetTotalTime();
		average = (float)totalTime / (float)mCurrentWorkdHoursCount;
		return average;
	}

	float TimeSheet::GetStandardDeviation() const
	{
		float variance = 0.0f;
		float std;
		float average;
		float difference;

		if (mCurrentWorkdHoursCount == 0)
		{
			return 0.0f;
		}

		average = this->GetAverageTime();
		for (unsigned int i = 0; i < mCurrentWorkdHoursCount; i++)
		{
			difference = (float)mWorkedHours[i] - average;
			variance += (difference * difference);
		}
		variance /= float(mCurrentWorkdHoursCount);

		std = std::sqrt(variance);
		return std;
	}

	const std::string& TimeSheet::GetName() const
	{
		return mName;
	}
}